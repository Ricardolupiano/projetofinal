/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.util.ArrayList;
import javax.swing.JTable;
import static javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS;
import javax.swing.table.DefaultTableModel;
import model.Despesa;
import static view.TelaDadosDespesas.umControleDespesa;


public class TelaDespesas extends javax.swing.JFrame {

    int mes1=1;
    int ano1=2013;
    int mes2=7;
    int ano2=2014;
    
    public TelaDespesas() {
        initComponents();
        carregarLista();
        jComboBox_Mes2.setSelectedIndex(mes2-1);
        jComboBox_Ano2.setSelectedItem("2014");
    }
private void carregarLista()
    {
        boolean permissao;
        ArrayList<Despesa> listaDespesas = umControleDespesa.getListaGasto();
        DefaultTableModel model = (DefaultTableModel) jTable_Despesa.getModel();
        model.setRowCount(0);
        for (Despesa d : listaDespesas) 
        {
            permissao=false;
            if(d.getAno()>=ano1&&d.getAno()<=ano2)
                permissao=true;
            else if(d.getAno()==ano1&&d.getAno()==ano2&&d.getMes()>=mes1&&d.getMes()<=mes2)
                permissao=true;
            else if(d.getAno()==ano1&&d.getAno()!=ano2&&d.getMes()>=mes1)
                permissao=true;
            else if(d.getAno()!=ano1&&d.getAno()==ano2&&d.getMes()<=mes2)
                permissao=true;
            if(d.getAno()<ano1)
                permissao=false;
            else if(d.getAno()>ano2)
                permissao=false;
            else if(d.getAno()==ano1&&d.getMes()<mes1)
                permissao=false;
            else if(d.getAno()==ano2&&d.getMes()>mes2)
                permissao=false;
            
            if(permissao==true)
                model.addRow(new String[]{d.getNome(),Integer.toString(d.getDia()),Integer.toString(d.getMes()),Integer.toString(d.getAno()),d.getDescricao(),Double.toString(d.getValor())});     
        }
        jTable_Despesa.setModel(model);  
  
        jTable_Despesa.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);  
        jTable_Despesa.getColumnModel().getColumn(0).setPreferredWidth(220);  
        jTable_Despesa.getColumnModel().getColumn(1).setPreferredWidth(50);  
        jTable_Despesa.getColumnModel().getColumn(2).setPreferredWidth(50);  
        jTable_Despesa.getColumnModel().getColumn(3).setPreferredWidth(50);  
        jTable_Despesa.getColumnModel().getColumn(4).setPreferredWidth(533);
        jTable_Despesa.getColumnModel().getColumn(4).setPreferredWidth(222);   
  
        model.fireTableDataChanged();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton_Sair = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_Despesa = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jComboBox_Mes1 = new javax.swing.JComboBox();
        jComboBox_Ano1 = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jComboBox_Mes2 = new javax.swing.JComboBox();
        jComboBox_Ano2 = new javax.swing.JComboBox();
        jRadioButton1 = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Histórico de Despesas");
        setBounds(new java.awt.Rectangle(300, 300, 0, 0));

        jButton_Sair.setText("Sair");
        jButton_Sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_SairActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel1.setText("Histórico de Despesas");

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel2.setText("Período:");

        jLabel3.setText("De");

        jTable_Despesa.setModel(new javax.swing.table.DefaultTableModel
            (
                null,
                new String [] {
                    "Histórico", "Dia", "Mês", "Ano", "Observação", "Valor"
                }
            )
            {
                @Override
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jScrollPane1.setViewportView(jTable_Despesa);

            jButton1.setText("Nova Despesa");
            jButton1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton1ActionPerformed(evt);
                }
            });

            jComboBox_Mes1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" }));
            jComboBox_Mes1.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    jComboBox_Mes1ItemStateChanged(evt);
                }
            });

            jComboBox_Ano1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2013", "2014", "2015", "2016", "2017" }));
            jComboBox_Ano1.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    jComboBox_Ano1ItemStateChanged(evt);
                }
            });

            jLabel4.setText("Até");

            jComboBox_Mes2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" }));
            jComboBox_Mes2.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    jComboBox_Mes2ItemStateChanged(evt);
                }
            });

            jComboBox_Ano2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2013", "2014", "2015", "2016", "2017" }));
            jComboBox_Ano2.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    jComboBox_Ano2ItemStateChanged(evt);
                }
            });

            jRadioButton1.setText("Mostrar Todas Despesas");
            jRadioButton1.addItemListener(new java.awt.event.ItemListener() {
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    jRadioButton1ItemStateChanged(evt);
                }
            });

            javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
            jPanel1.setLayout(jPanel1Layout);
            jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addGap(48, 48, 48)
                            .addComponent(jRadioButton1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jButton_Sair)
                                    .addGap(135, 135, 135)
                                    .addComponent(jLabel1))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 673, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(21, 21, 21)
                                    .addComponent(jLabel2)
                                    .addGap(48, 48, 48)
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jComboBox_Mes1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jComboBox_Ano1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(31, 31, 31)
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jComboBox_Mes2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jComboBox_Ano2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(0, 7, Short.MAX_VALUE)))
                    .addContainerGap())
            );
            jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton_Sair)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel1)))
                    .addGap(40, 40, 40)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(jLabel3)
                        .addComponent(jComboBox_Mes1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jComboBox_Ano1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(jComboBox_Mes2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jComboBox_Ano2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jRadioButton1))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
            );

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
            );

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void jButton_SairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_SairActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton_SairActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
        new TelaDadosDespesas().setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jRadioButton1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jRadioButton1ItemStateChanged
        if(jRadioButton1.isSelected())
        {
            mes1=1;
            mes2=12;
            ano1=2013;
            ano2=2017;
            jComboBox_Ano1.setEnabled(false);
            jComboBox_Ano2.setEnabled(false);
            jComboBox_Mes1.setEnabled(false);
            jComboBox_Mes2.setEnabled(false);
        }
        else
        {
            mes1=jComboBox_Mes1.getSelectedIndex()+1;
            mes2=jComboBox_Mes2.getSelectedIndex()+1;
            ano1=Integer.parseInt((String) jComboBox_Ano1.getSelectedItem());
            ano2=Integer.parseInt((String) jComboBox_Ano1.getSelectedItem());
            jComboBox_Ano1.setEnabled(true);
            jComboBox_Ano2.setEnabled(true);
            jComboBox_Mes1.setEnabled(true);
            jComboBox_Mes2.setEnabled(true);
        }
        carregarLista();
    }//GEN-LAST:event_jRadioButton1ItemStateChanged

    private void jComboBox_Mes1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox_Mes1ItemStateChanged
        mes1=jComboBox_Mes1.getSelectedIndex()+1;
        carregarLista();
    }//GEN-LAST:event_jComboBox_Mes1ItemStateChanged

    private void jComboBox_Ano1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox_Ano1ItemStateChanged
        ano1=Integer.parseInt((String) jComboBox_Ano1.getSelectedItem());
        carregarLista();
    }//GEN-LAST:event_jComboBox_Ano1ItemStateChanged

    private void jComboBox_Mes2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox_Mes2ItemStateChanged
        mes2=jComboBox_Mes2.getSelectedIndex()+1;
        carregarLista();
    }//GEN-LAST:event_jComboBox_Mes2ItemStateChanged

    private void jComboBox_Ano2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox_Ano2ItemStateChanged
        ano2=Integer.parseInt((String) jComboBox_Ano2.getSelectedItem());
        carregarLista();
    }//GEN-LAST:event_jComboBox_Ano2ItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaDespesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaDespesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaDespesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaDespesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaDespesas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton_Sair;
    private javax.swing.JComboBox jComboBox_Ano1;
    private javax.swing.JComboBox jComboBox_Ano2;
    private javax.swing.JComboBox jComboBox_Mes1;
    private javax.swing.JComboBox jComboBox_Mes2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable_Despesa;
    // End of variables declaration//GEN-END:variables
}
